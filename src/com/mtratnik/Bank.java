package com.mtratnik;

import java.text.BreakIterator;
import java.util.ArrayList;

public class Bank {
    // You job is to create a simple banking application.
    // There should be a Bank class
    // It should have an arraylist of Branches
    // Each Branch should have an arraylist of Customers
    // The Customer class should have an arraylist of Doubles (transactions)
    // Customer:
    // Name, and the ArrayList of doubles.
    // Branch:
    // Need to be able to add a new customer and initial transaction amount.
    // Also needs to add additional transactions for that customer/branch
    // Bank:
    // Add a new branch
    // Add a customer to that branch with initial transaction
    // Add a transaction for an existing customer for that branch
    // Show a list of customers for a particular branch and optionally a list
    // of their transactions
    // Demonstration autoboxing and unboxing in your code
    // Hint: Transactions
    // Add data validation.
    // e.g. check if exists, or does not exist, etc.
    // Think about where you are adding the code to perform certain actions

    private ArrayList<Branch> branchesArrayList;

    public Bank() {
        this.branchesArrayList = new ArrayList<Branch>();
    }

    public boolean addBranch(String branchName) {
        if (findBranchInt(branchName) < 0) {
            Branch branch = new Branch(branchName);
            branchesArrayList.add(branch);
            System.out.println("Creating new branch: " + branchName);
            return true;
        } else {
            System.out.println("Branch already exists");
            return false;
        }
    }

    public void customerOperation(String branchName, String customerName, double initialAmount) {
        if (findBranchInt(branchName) < 0) {
            addBranch(branchName);
        }

        Branch branch = branchesArrayList.get(findBranchInt(branchName));
        branch.customerOperation(customerName, initialAmount);
    }

    public void listCustomers(String branchName) {
        if (findBranchInt(branchName) < 0) {
            System.out.println("Branch does not exist.");
        } else {
            Branch branch = branchesArrayList.get(findBranchInt(branchName));
            branch.showCustomerList();
//            branch.showCustomerTransactions();
        }
    }

    private int findBranchInt(String branchName) {
        for (int i = 0; i < branchesArrayList.size(); i++) {
            Branch branch = branchesArrayList.get(i);
            if (branchName.equals(branch.getBranchName())) {
                return i;
            }
        }

        return -1;
    }
}
