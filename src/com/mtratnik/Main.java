package com.mtratnik;

import javax.swing.*;
import java.rmi.server.RMIFailureHandler;
import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);
    private static Bank bank = new Bank();

    public static void main(String[] args) {
        // You job is to create a simple banking application.
        // There should be a Bank class
        // It should have an arraylist of Branches
        // Each Branch should have an arraylist of Customers
        // The Customer class should have an arraylist of Doubles (transactions)
        // Customer:
        // Name, and the ArrayList of doubles.
        // Branch:
        // Need to be able to add a new customer and initial transaction amount.
        // Also needs to add additional transactions for that customer/branch
        // Bank:
        // Add a new branch
        // Add a customer to that branch with initial transaction
        // Add a transaction for an existing customer for that branch
        // Show a list of customers for a particular branch and optionally a list
        // of their transactions
        // Demonstration autoboxing and unboxing in your code
        // Hint: Transactions
        // Add data validation.
        // e.g. check if exists, or does not exist, etc.
        // Think about where you are adding the code to perform certain actions
        boolean operation = true;
        int option = 1;
        optionsList();

        while (operation) {

            System.out.println("\nPick one of the available options:");
            option = scanner.nextInt();
            scanner.nextLine();

            switch (option) {
                case 1:
                    optionsList();
                    break;
                case 2:
                    addBranch();
                    break;
                case 5:
                    System.out.println("Thank you for using our application.");
                    operation = false;
            }
        }
    }

    private static void optionsList() {
        System.out.println("1 - List all available options.");
        System.out.println("2 - Add a branch.");
        System.out.println("3 - Add a new customer or new transaction to an existing customer.");
        System.out.println("4 - Get customer list and transaction for a branch.");
        System.out.println("5 - Quit");
    }

    private static void addBranch() {
        System.out.println("Please enter a name of new branch: ");
        bank.addBranch(scanner.nextLine());
    }
}
