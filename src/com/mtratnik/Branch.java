package com.mtratnik;

import java.util.ArrayList;

public class Branch {

    private String branchName;
    private ArrayList<Customer> customersArrayList;

    public Branch(String branchName) {
        this.branchName = branchName;
        this.customersArrayList = new ArrayList<Customer>();
    }

    public String getBranchName() {
        return branchName;
    }

    public ArrayList<Customer> getCustomersArrayList() {
        return customersArrayList;
    }

    public void customerOperation(String customerName, double operationAmount) {
        if (getCustomer(customerName) < 0) {
            addNewCustomer(customerName, operationAmount);
            System.out.println("Customer " + customerName + " added to " + this.branchName + " branch with initial amount of " + operationAmount + " EUR.");
        } else {
            Customer customer = customersArrayList.get(getCustomer(customerName));
            customer.addTransaction(operationAmount);
            System.out.println("Transaction of " + operationAmount + " added to customer " + customerName);
        }
    }

    public void showCustomerList() {
        for (int i = 0; i < customersArrayList.size(); i++) {
            Customer customer = customersArrayList.get(i);
            System.out.println("Customer no. " + (i+1) + " is " + customer.getCustomerName() + " with following transactions: " + customer.getCustomerTransactions());
        }
    }

//    public void showCustomerTransactions() {
//        for (int i = 0; i < customersArrayList.size(); i++) {
//            Customer customer = customersArrayList.get(i);
//            System.out.println("Customer " + customer.getCustomerName() + " made following transactions " + customer.getCustomerTransactions());
//        }
//    }

    private void addNewCustomer(String customerName, double initialAmount) {
        Customer customer = new Customer(customerName, initialAmount);
        customersArrayList.add(customer);
    }

    private int getCustomer(String customerName) {
        for (int i = 0; i < customersArrayList.size(); i++) {
            Customer customer = customersArrayList.get(i);
            if (customerName.equals(customer.getCustomerName())) {
                return i;
            }
        }

        return -1;
    }

}
