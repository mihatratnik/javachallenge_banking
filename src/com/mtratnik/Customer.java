package com.mtratnik;

import java.util.ArrayList;

public class Customer {

    private String customerName;
    private ArrayList<Double> customerTransactions;

    public Customer(String customerName, double customerTransaction) {
        this.customerName = customerName;
        this.customerTransactions = new ArrayList<Double>();
        this.customerTransactions.add(customerTransaction);
    }

    public String getCustomerName() {
        return customerName;
    }

    public ArrayList<Double> getCustomerTransactions() {
        return customerTransactions;
    }

    public void addTransaction(double transactionAmount) {
        customerTransactions.add(transactionAmount);
    }

}
